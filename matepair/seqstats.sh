#!/bin/bash
#PBS -l nodes=1:ppn=1:thinnode,walltime=24:00:00
#PBS -W group_list=cu_10027 -A cu_10027
#PBS -m n
#PBS -j eo
#PBS -N seqstats_mp_rpe
#PBS -V

module load samtools/1.8
module load R/3.2.5
module load cov_tool/0.5.6
module load seqstats/0.5.6

base_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/mate_pair
fastq_dir=/home/projects/cu_10027/projects/rpe/data/data_raw/mate_pair
index=/home/projects/cu_10027/data/genomes/hg38/ncbi/hg38.fa
tmp_dir=/home/projects/cu_10027/scratch

sample=`ls -1 $fastq_dir | sed -n ${PBS_ARRAYID}p`

mkdir -p $base_dir/qc/$sample

run_seqstats --bam $base_dir/bam/$sample/${sample}.bam --out $base_dir/qc/$sample/$sample --fasta $index --tmp $tmp_dir -g /home/projects/cu_10027/data/genomes/hg38/ncbi/GCA_000001405.15_GRCh38_no_alt_analysis_set.gc
