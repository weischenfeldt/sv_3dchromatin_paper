# Libraries and arguments -------------------------------------------------

if (!suppressMessages(require("pacman")))
  install.packages("pacman")

p_load(optparse)

option_list <- list(
  make_option(c("-s", "--snp"), type = "character", default = NULL,
              help = "Phased snps", metavar = "character"),
  make_option(c("-o", "--output"), type = "character", default = NULL,
              help = "Output file (stdout if not specified)", metavar = "character"),
  make_option(c("--append"), type = "logical", default = FALSE, action = "store_true",
              help = "Output file (stdout if not specified)", metavar = "character")
)

opt_parser <- OptionParser(option_list = option_list)
opt <- parse_args(opt_parser)

p_load(tidyverse)
p_load(stringi)

# Functions ---------------------------------------------------------------

edit_seq <- function(seq, cigar, pos = 0) {

  len <- parse_number(cigar)
  flag <- substr(cigar, str_length(len) + 1, str_length(len) + 1)

  if (flag %in% c("M", "H")) {
    pos <- pos + as.integer(len)
  } else if (flag == "S") {
    substr(seq, pos + 1, len + pos + 1) <- str_flatten(rep(".", len))
    pos <- pos + len
  } else if (flag == "D") {
    stri_sub(seq, len + pos + 1, pos + 1) <- str_flatten(rep(".", len))
    pos <- pos + len
  } else if (flag == "I") {
    seq <- str_flatten(c(substr(seq, 1, pos), substr(seq, len + pos + 1, str_length(seq))))
  } else {
    print(cigar)
    stop("Unexpected CIGAR string")
  }

  cigar <- substr(cigar, str_length(len) + 2, str_length(cigar))

  if (str_length(cigar) > 0) {
    edit_seq(seq, cigar, pos)
  } else {
    return(seq)
  }

}

annotate_snps <- function(seq, chromosome, position, snps, read = 1) {

  hits <- snps[[line[2]]] %>% filter(between(pos, position, position + str_length(seq)))

  if (read == 1) {
    snp <- "SNP1"
  } else {
    snp <- "SNP2"
  }

  if (nrow(hits) > 0) {
    for (i in 1:nrow(hits)) {
      cur_snp <- substr(seq, hits$pos[i] - position + 1, hits$pos[i] - position + 1)
      if (cur_snp == hits$mat[i]) {
        snp <- paste(snp, cur_snp, hits$pos[i], "maternal", sep = ":")
      } else if (cur_snp == hits$pat[i]) {
        snp <- paste(snp, cur_snp, hits$pos[i], "paternal", sep = ":")
      }
    }
  }

  return(snp)
}

# Main --------------------------------------------------------------------

if (tolower(tools::file_ext(opt$snp)) %in% c("rda", "rds")) {
  snps <- readRDS(opt$snp)
} else {
  snps <- read_delim(opt$snp, delim = " ", col_names = c("a", "pat", "mat")) %>%
    separate(a, into = c("chr", "pos")) %>% mutate(pos = as.integer(pos))

  snp_list <- list()
  for (i in paste0("chr", c(1:22, X))) { snp_list[[i]] <- snps %>% filter(chr == i) }
  snps <- snp_list
}


f <- file("stdin")
open(f)
while(length(line <- readLines(f, n = 1)) > 0) {

  line <- unlist(str_split(line, " "))

  seq1 <- edit_seq(line[11], line[10])
  seq2 <- edit_seq(line[14], line[13])

  if (line[1] != 0) {
    pos1 <- as.integer(line[3]) - str_length(seq1) + 1
  } else {
    pos1 <- as.integer(line[3])
  }

  if (line[5] != 0) {
    pos2 <- as.integer(line[7]) - str_length(seq2) + 1
  } else {
    pos2 <- as.integer(line[7])
  }

  snp1 <- annotate_snps(seq1, line[2], pos1, snps, 1)
  snp2 <- annotate_snps(seq2, line[6], pos2, snps, 2)

  if (snp1 != "SNP1" || snp2 != "SNP2") {
    line_out <- paste(c(line[c(1:8, 11, 14, 15)], snp1, snp2), collapse = " ")
    write(line_out, stdout())
  }
}


