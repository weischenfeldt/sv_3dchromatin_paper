#!/bin/bash
#PBS -l nodes=1:ppn=1:thinnode,mem=2gb,walltime=48:00:00
#PBS -W group_list=cu_10027 -A cu_10027
#PBS -m n
#PBS -j eo
#PBS -N diploid_mnd
#PBS -V

module load tools
module load parallel/20170822
module load java/1.8.0

cd $PBS_O_WORKDIR

sample=$1
chroms=`echo chr{1..22} chrX`
chrom=`echo $chroms | cut -f $PBS_ARRAYID -d " "`

export juiceDir=/home/projects/cu_10027/apps/software/juicer
export base_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38
export diploid_dir=$base_dir/phasing/diploid
export mnd=$base_dir/juicer/work/$sample/aligned/merged_nodups.txt.gz
export outdir=$base_dir/juicer/work/$sample/aligned/diploid
export scripts=/home/projects/cu_10027/projects/rpe/code/code_raw

mkdir -p $outdir

split_diploid() {
  zcat $mnd | awk -v chr=$1 '$2==chr && $6==chr && $9 >= 10 && $12 >= 10' - | \
    perl $juiceDir/CPU/common/diploid.pl -s $diploid_dir/RPE-1_chr_pos_${1}.txt \
    -o $diploid_dir/RPE-1_paternal_maternal_${1}.txt > $outdir/diploid_${1}.mnd.txt
}

export -f split_diploid

cd $outdir

split_diploid $chrom
awk -v chr=$chrom -f $scripts/phasing/annotate_haplotype.awk $outdir/diploid_${chrom}.mnd.txt | \
  awk '{if ($3<$7) print $11,$5,$$6,$7,$8,$1,$2,$3,$4,100,100; else print $0;}' |
  sort -S 2G -k3,3V -k7,7V - > $outdir/mat_pat_${chrom}.txt
