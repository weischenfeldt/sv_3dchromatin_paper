#!/bin/bash
#PBS -l nodes=1:ppn=28:thinnode,walltime=72:00:00
#PBS -W group_list=cu_10027 -A cu_10027
#PBS -m n
#PBS -j eo
#PBS -N diploid_prep
#PBS -V

module load parallel/20170822
module load intel/redist/2019_update2
module load intel/perflibs/64
module load R/3.5.0

cd $PBS_O_WORKDIR

mnd=$1
outdir=$2
RPE1_phased=/home/projects/cu_10027/projects/rpe/code/code_raw/phasing/test_files/RPE-1_paternal_maternal.rda

mkdir -p $outdir/tmp

zcat $mnd | parallel -j 28 --will-cite --block 100M --pipe "Rscript /home/projects/cu_10027/projects/rpe/code/code_raw/phasing/annotate_snps_stdin.R -s $RPE1_phased > $outdir/tmp/diploid.file{#}.txt"

#cat tmp/* | sort --parallel=28 -k2,2d -k6,6d -k4,4n -k8,8n -k1,1n -k5,5n -k3,3n - > diploid.mnd.txt
cat tmp/*  > diploid.mnd.txt

awk -f /home/projects/cu_10027/projects/rpe/code/code_raw/phasing/annotate_haplotype.awk diploid.mnd.txt | \
  awk '{if ($3>$7) {print $1,$6,$7,$8,$9,$2,$3,$4,$5,$10,$11} else {print $0}}' | sort --parallel=4 -k3,3V -k7,7V - > mat_pat.txt
