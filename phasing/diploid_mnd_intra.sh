#!/bin/bash
#PBS -l nodes=1:ppn=23:thinnode,walltime=48:00:00
#PBS -W group_list=cu_10027 -A cu_10027
#PBS -m n
#PBS -j eo
#PBS -N diploid_mnd
#PBS -V

module load tools
module load parallel/20170822

cd $PBS_O_WORKDIR
sample=$1

export juiceDir=/home/projects/cu_10027/apps/software/juicer
export base_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38
export diploid_dir=$base_dir/phasing/diploid
export mnd=$base_dir/juicer/work/$sample/aligned/merged_nodups.txt.gz
export outdir=$base_dir/juicer/work/$sample/aligned/diploid

mkdir -p $outdir

chroms=`echo chr{1..22} chrX`

split_diploid() {
  zcat $mnd | awk -v chr=$1 '$2==chr && $6==chr && $9 >= 10 && $12 >= 10' - | \
    perl $juiceDir/CPU/common/diploid.pl -s $diploid_dir/RPE-1_chr_pos_${1}.txt \
    -o $diploid_dir/RPE-1_paternal_maternal_${1}.txt > $outdir/diploid_${1}.mnd.txt

  echo Done splitting ${1}
}

export -f split_diploid

parallel -j 23 split_diploid {} ::: $chroms

cat $outdir/diploid_chr{1..22}.mnd.txt $outdir/diploid_chrX.mnd.txt > $outdir/diploid.mnd.txt
rm $outdir/diploid_chr*.txt
