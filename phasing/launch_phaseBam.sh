#!/bin/bash
#PBS -l nodes=1:ppn=28:thinnode,walltime=48:00:00
#PBS -W group_list=cu_10027 -A cu_10027
#PBS -m n
#PBS -j eo
#PBS -N phaseSplitBam
#PBS -V

module load parallel/20170822
module load samtools/1.8
module load intel/redist/2019_update2
module load intel/perflibs/64
module load R/3.5.0

base_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/$1
sample=`ls $base_dir/bam | sed -n ${PBS_ARRAYID}p`

mkdir -p $base_dir/phased_bam/$sample

cd $base_dir/phased_bam/$sample
mkdir -p tmp

phase() {

  num=$1
  sam=$2
  scripts=/home/projects/cu_10027/projects/rpe/code/code_raw/phasing

  cat $sam | Rscript $scripts/phaseBam.R -s $scripts/test_files/RPE-1_paternal_maternal.rda | \
    awk -v num=$num -F "\t" 'OFS="\t" {if ($(NF) == "mat") {$NF=""; print >> "tmp/maternal_"num".sam" ;}
  else if ($(NF) == "pat") {$NF=""; print >> "tmp/paternal_"num".sam" ;}}'

}

export -f phase

samtools view -H $base_dir/bam/$sample/${sample}.bam > ${sample}.sam.header
samtools view -q 30 $base_dir/bam/$sample/${sample}.bam | \
  parallel -j 28 --block 100M --pipe "phase {#} - "

#cat ${sample}.sam.header <(ls tmp/unassigned_*| sort -k1,1V | xargs cat) | samtools view -Sb - > ${sample}.unassigned.bam &
cat ${sample}.sam.header <(ls tmp/paternal_*| sort -k1,1V | xargs cat) | samtools view -Sb - > ${sample}.paternal.bam &
cat ${sample}.sam.header <(ls tmp/maternal_*| sort -k1,1V | xargs cat) | samtools view -Sb - > ${sample}.maternal.bam &

wait

samtools index ${sample}.paternal.bam
samtools index ${sample}.maternal.bam

echo "Done!"
