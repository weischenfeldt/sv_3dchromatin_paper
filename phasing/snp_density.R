library(tidyverse)

setwd("~/Github/weischenfeldt/sv_3Dchromatin_paper")

snps <- read_tsv("RPE-1_Haplotype.txt.gz")
snp_dens <- snps %>%
  mutate(bin = pos %/% 10000) %>%
  group_by(chr, bin) %>% summarize(score = n()) %>%
  mutate(start = 10000 * bin, end = 10000 * bin + 10000L) %>%
  select(chr, start, end, score) %>% ungroup()

snp_dens %>%
  write_tsv("RPE-1_snp_density.bedgraph", col_names = FALSE)


bind_rows(snp_dens %>% mutate(chr = paste0(chr, "_mat")),
          snp_dens %>% mutate(chr = paste0(chr, "_pat"))) %>%
  write_tsv("RPE-1_diploid_snp_density.bedgraph",  col_names = FALSE)
