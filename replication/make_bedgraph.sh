#!/bin/bash

module load java/1.8.0
module load igvtools/2.3.98

sample=$1
base_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38
cd $base_dir/repliseq/profile/$sample

phases=`echo G1 S1 S2 S3 S4 G2`

for s in `ls *profile.tsv.gz`;
do
  sample=`basename $s .profile.tsv.gz`

  for i in {1..6};
  do
    phase=`echo $phases|awk -v i=$i '{print $i}'`

    zcat ${sample}.profile.tsv.gz  | \
      awk -v sample=$sample -v phase=$phase -v i="$i" \
        'BEGIN{OFS=""; print "track type=bedGraph name=", sample, "_", phase};
         NR > 1 {OFS="\t"; print $1, $2, $2 + 1000, $(i+2)}' | \
        gzip - >  ${sample}_${phase}_repliseq.bedGraph.gz

  done
done
