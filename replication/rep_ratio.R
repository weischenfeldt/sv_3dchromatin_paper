library(tidyverse)

a <- read_tsv("~/Github/weischenfeldt/sv_3Dchromatin_paper/data/replication/BM175.reptime.bedGraph.gz",
              col_names = c("chr", "start", "end", "timing"))
b <- read_tsv("~/Github/weischenfeldt/sv_3Dchromatin_paper/data/replication/C29.reptime.bedGraph.gz",
              col_names = c("chr", "start", "end", "timing"))

inner_join(a,b, by = c("chr", "start", "end")) %>%
  filter(chr %in% paste0("chr", c(1:22, "X"))) %>%
  mutate(timing.x = timing.x + 0.001, timing.y = timing.y + 0.001, aa = log(timing.x/timing.y)) %>%
  select(1:3, 6) %>%
  write_tsv("~/juicebox/rpe/hg38/repliseq/profile/BM175/BM175.reptime.logratio.bedGraph",
            col_names = FALSE)

out <- "~/Github/weischenfeldt/sv_3Dchromatin_paper/data/replication/BM175.dWA.bedGraph"

a %>%  filter(chr %in% paste0("chr", c(1:22, "X"))) %>%
  write_tsv(out, col_names = FALSE)

gzip(out, destname = sprintf("%s.gz", out), remove = TRUE)
