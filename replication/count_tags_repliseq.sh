#!/bin/bash
#PBS -l nodes=1:ppn=1:thinnode,walltime=24:00:00
#PBS -W group_list=cu_10027 -A cu_10027
#PBS -m n
#PBS -j eo
#PBS -N repliseq_count
#PBS -V

module load samtools/1.8

base_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/repliseq
index=/home/projects/cu_10027/data/genomes/hg38/ncbi/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna
tmp_dir=/home/projects/cu_10027/scratch

sample=`ls -1 $base_dir/bam | sed -n ${PBS_ARRAYID}p`

mkdir -p $base_dir/profile/$sample

script=/home/projects/cu_10027/apps/software/repliseq/src/repliseq

$script -r $index -o $base_dir/profile/$sample/$sample \
 $base_dir/bam/$sample/${sample}_G1.bam \
 $base_dir/bam/$sample/${sample}_S1.bam \
 $base_dir/bam/$sample/${sample}_S2.bam \
 $base_dir/bam/$sample/${sample}_S3.bam \
 $base_dir/bam/$sample/${sample}_S4.bam \
 $base_dir/bam/$sample/${sample}_G2.bam
