#!/bin/bash
#PBS -l nodes=1:ppn=8:thinnode,walltime=24:00:00
#PBS -W group_list=cu_10027 -A cu_10027
#PBS -m n
#PBS -j eo
#PBS -N repliseq_map
#PBS -V

module load bwa/0.7.16a
module load samtools/1.8

base_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/repliseq
fastq_dir=/home/projects/cu_10027/projects/rpe/data/data_raw/repliseq
index=/home/projects/cu_10027/data/genomes/hg38/ncbi/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna
tmp_dir=/home/projects/cu_10027/scratch

sample=`ls -1 $fastq_dir | sed -n ${PBS_ARRAYID}p`

mkdir -p $base_dir/bam/$sample

for i in G{1..2} S{1..4};
do
  bwa mem -t 8 $index $fastq_dir/$sample/${sample}_${i}.fastq.gz | \
    samtools sort -@ 8 -O bam -o $base_dir/bam/$sample/${sample}_${i}.bam

  samtools index -@ 8 $base_dir/bam/$sample/${sample}_${i}.bam
done
