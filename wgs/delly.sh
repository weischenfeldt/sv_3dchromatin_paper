#!/bin/bash

#PBS -l nodes=1:ppn=1:thinnode,walltime=96:00:00
#PBS -W group_list=cu_10027 -A cu_10027
#PBS -m n
#PBS -j eo
#PBS -N delly_rpe
#PBS -V

module purge
module load torque moab tools gcc/5.4.0 R/3.2.5
module load delly/0.7.7

sample=$1
type=$2
genome=/home/projects/cu_10027/data/genomes/hg38/ncbi/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna
excl=/home/projects/cu_10027/data/genomes/gencode/human.hg38_excl.tsv
bam_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/wgs/bam
out_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/wgs/delly

mkdir -p $out_dir

delly call -t $type -g $genome -x $excl -o $out_dir/${sample}/${sample}_${type}.bcf \
  $bam_dir/${sample}/${sample}.bam $bam_dir/RPEWTp30/RPEWTp30.bam
