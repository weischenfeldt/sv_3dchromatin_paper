#!/bin/bash
#PBS -l nodes=1:ppn=14:thinnode,walltime=24:00:00
#PBS -W group_list=cu_10027 -A cu_10027
#PBS -m n
#PBS -j eo
#PBS -N bwa_wgs_rpe
#PBS -V

module load bwa/0.7.16a
module load samtools/1.8

base_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/wgs
fastq_dir=/home/projects/cu_10027/projects/rpe/data/data_raw/wgs
index=/home/projects/cu_10027/data/genomes/hg38/ncbi/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna
tmp_dir=/home/projects/cu_10027/scratch

sample=`ls -1 $fastq_dir | sed -n ${PBS_ARRAYID}p`

mkdir -p $base_dir/bam/$sample

bwa mem -t 14 -T 0 -M $index $fastq_dir/$sample/${sample}_R1.fastq.gz $fastq_dir/$sample/${sample}_R2.fastq.gz | \
  samtools sort -l 1 -@ 14 -O bam -o $base_dir/bam/$sample/${sample}.bam

samtools index -@ 14 $base_dir/bam/$sample/${sample}.bam
