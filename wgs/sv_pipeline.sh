#!/bin/bash

module add pype

root_pype=/home/people/nikossi/software/pype_weischenfeldt_computerome
export PYPE_PROFILES=$root_pype/profiles
export PYPE_PIPELINES=$root_pype/pipelines
export PYPE_SNIPPETS=$root_pype/snippets
export PYPE_QUEUES=$root_pype/queues

base_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/wgs
tumor_sample=$1
normal_sample=RPEWTp30

logdir=$base_dir/log/$tumor_sample
mkdir -p $base_dir/cov/$tumor_sample
mkdir -p $base_dir/delly/$tumor_sample
mkdir -p $base_dir/delly/$tumor_sample/plots/circos
mkdir -p $base_dir/delly/$tumor_sample/plots/highconf
mkdir -p $base_dir/delly/$tumor_sample/plots/raw
mkdir -p $base_dir/sequenza/$tumor_sample/seqz

mkdir -p $logdir

pype -p hg38 pipelines --log $logdir sv_pipeline \
    --out_circos_plot $base_dir/delly/$tumor_sample/plots/circos \
    --out_cov_plot_high $base_dir/delly/$tumor_sample/plots/highconf \
    --out_cov_plot_raw $base_dir/delly/$tumor_sample/plots/raw \
    --tumor_bam $base_dir/bam/$tumor_sample/${tumor_sample}.bam \
    --normal_bam $base_dir/bam/$normal_sample/${normal_sample}.bam \
    --out_filter $base_dir/delly/$tumor_sample \
    --sample_name $tumor_sample \
    --out_vcf $base_dir/delly/$tumor_sample/$tumor_sample \
    --normal_cov $base_dir/cov/$tumor_sample/${normal_sample}.cov \
    --tumor_cov $base_dir/cov/$tumor_sample/${tumor_sample}.cov \
    --seqz_out $base_dir/sequenza/$tumor_sample/seqz/$tumor_sample \
    --sequenza_out $base_dir/sequenza/$tumor_sample --x_heterozygous True
