#!/bin/bash

cd /home/projects/cu_10027/projects/rpe/code/code_raw/chipseq
base_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/chipseq/GSE60024

#mkdir -p $base_dir/{RPE-H3K27ac,RPE-CTCF,RPE-POLII}/aquas_chipseq

qsub -e $base_dir/RPE-H3K27ac/aquas_chipseq/RPE-H3K27ac.log GSE60024.sh -F "SRR1534806 RPE-H3K27ac histone"
qsub -e $base_dir/RPE-CTCF/aquas_chipseq/RPE-CTCF.log GSE60024.sh -F "SRR1534808 RPE-CTCF TF"
qsub -e $base_dir/RPE-POLII/aquas_chipseq/RPE-POLII.log GSE60024.sh -F "SRR1534810 RPE-POLII TF"
