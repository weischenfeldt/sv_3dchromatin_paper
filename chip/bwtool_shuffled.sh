#!/bin/bash

module load bwtool/20170921

cd /home/projects/cu_10027/projects/rpe/data/data_processed/hg38/sv/delly_liftover/bed/shuffled

out_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/sv/delly_liftover/footprints
res=50000
tile=250

GSE60024_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/chipseq/GSE60024
signal_dir=aquas_chipseq/signal/macs2/rep1
#CTCF
for i in {1..100};
do
  bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=CTCF breakpoints_all_shuffled_${i}.bed $GSE60024_dir/RPE-CTCF/${signal_dir}/SRR1534808.nodup.tagAlign_x_SRR1534805.nodup.tagAlign.fc.signal.bw ctcf_shuffled_footprint_res_${res}_tile_${tile}_${i}.txt

  #H3K27ac
  bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=PolII breakpoints_all_shuffled_${i}.bed $GSE60024_dir/RPE-H3K27ac/${signal_dir}/SRR1534806.nodup.tagAlign_x_SRR1534805.nodup.tagAlign.fc.signal.bw h3k27ac_shuffled_footprint_res_${res}_tile_${tile}_${i}.txt

  #PolII
  bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=PolII breakpoints_all_shuffled_${i}.bed $GSE60024_dir/RPE-POLII/${signal_dir}/SRR1534810.nodup.tagAlign_x_SRR1534805.nodup.tagAlign.fc.signal.bw polII_shuffled_footprint_res_${res}_tile_${tile}_${i}.txt
done

cat ctcf_shuffled_footprint_res_${res}_tile_${tile}_*.txt | gzip - > $out_dir/ctcf_shuffled_footprint_res_${res}_tile_${tile}.txt
cat h3k27ac_shuffled_footprint_res_${res}_tile_${tile}_*.txt | gzip - > $out_dir/h3k27ac_shuffled_footprint_res_${res}_tile_${tile}.txt
cat polII_shuffled_footprint_res_${res}_tile_${tile}_*.txt | gzip - > $out_dir/polII_shuffled_footprint_res_${res}_tile_${tile}.txt
