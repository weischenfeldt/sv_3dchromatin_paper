#!/bin/bash

module load bwtool/20170921

base_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/sv/delly_liftover/bed

cd $base_dir

out_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/sv/delly_liftover/footprints
perm_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/sv/delly_liftover/footprints/permutations
shuffled_dir=$base_dir/shuffled
wig_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/rnaseq/wig

mkdir -p $perm_dir $out_dir $shuffled_dir

### Shuffle bed
for i in {1..100}; do
  bedtools shuffle -excl /home/projects/cu_10027/projects/rpe/data/data_processed/hg38/annotations/human.hg38.excl.tsv \
  -chrom -noOverlapping -i breakpoints_all.bed -g /home/projects/cu_10027/data/genomes/hg38/ncbi/hg38.chrom.sizes | \
  sortBed -i > $shuffled_dir/breakpoints_all_shuffled_${i}.bed;

  bedtools shuffle -excl /home/projects/cu_10027/projects/rpe/data/data_processed/hg38/annotations/human.hg38.excl.tsv \
  -chrom -noOverlapping -i breakpoints_C29.bed -g /home/projects/cu_10027/data/genomes/hg38/ncbi/hg38.chrom.sizes | \
  sortBed -i > $shuffled_dir/breakpoints_C29_shuffled_${i}.bed;

  bedtools shuffle -excl /home/projects/cu_10027/projects/rpe/data/data_processed/hg38/annotations/human.hg38.excl.tsv \
  -chrom -noOverlapping -i breakpoints_DCB2.bed -g /home/projects/cu_10027/data/genomes/hg38/ncbi/hg38.chrom.sizes | \
  sortBed -i > $shuffled_dir/breakpoints_DCB2_shuffled_${i}.bed;
done


### Footprints

res=50000
tile=250

GSE60024_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/chipseq/GSE60024
signal_dir=aquas_chipseq/signal/macs2/rep1
#CTCF
bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=CTCF breakpoints_all.bed $GSE60024_dir/RPE-CTCF/${signal_dir}/SRR1534808.nodup.tagAlign_x_SRR1534805.nodup.tagAlign.fc.signal.bw $out_dir/ctcf_breaks_footprint_res_${res}_tile_${tile}.txt
#bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=CTCF breakpoints_all_shuffled.bed $GSE60024_dir/RPE-CTCF/${signal_dir}/SRR1534808.nodup.tagAlign_x_SRR1534805.nodup.tagAlign.fc.signal.bw ctcf_shuffled_footprint_res_${res}_tile_${tile}.txt

#H3K27ac
bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=PolII breakpoints_all.bed $GSE60024_dir/RPE-H3K27ac/${signal_dir}/SRR1534806.nodup.tagAlign_x_SRR1534805.nodup.tagAlign.fc.signal.bw $out_dir/h3k27ac_breaks_footprint_res_${res}_tile_${tile}.txt
#bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=PolII breakpoints_all_shuffled.bed $GSE60024_dir/RPE-H3K27ac/${signal_dir}/SRR1534806.nodup.tagAlign_x_SRR1534805.nodup.tagAlign.fc.signal.bw h3k27ac_shuffled_footprint_res_${res}_tile_${tile}.txt

#PolII
bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=PolII breakpoints_all.bed $GSE60024_dir/RPE-POLII/${signal_dir}/SRR1534810.nodup.tagAlign_x_SRR1534805.nodup.tagAlign.fc.signal.bw $out_dir/polII_breaks_footprint_res_${res}_tile_${tile}.txt
#bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=PolII breakpoints_all_shuffled.bed $GSE60024_dir/RPE-POLII/${signal_dir}/SRR1534810.nodup.tagAlign_x_SRR1534805.nodup.tagAlign.fc.signal.bw polII_shuffled_footprint_res_${res}_tile_${tile}.txt

for i in {1..100};
do
  #CTCF
  bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=CTCF $shuffled_dir/breakpoints_all_shuffled_${i}.bed $GSE60024_dir/RPE-CTCF/${signal_dir}/SRR1534808.nodup.tagAlign_x_SRR1534805.nodup.tagAlign.fc.signal.bw $perm_dir/ctcf_shuffled_footprint_res_${res}_tile_${tile}_${i}.txt

  #H3K27ac
  bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=PolII $shuffled_dir/breakpoints_all_shuffled_${i}.bed $GSE60024_dir/RPE-H3K27ac/${signal_dir}/SRR1534806.nodup.tagAlign_x_SRR1534805.nodup.tagAlign.fc.signal.bw $perm_dir/h3k27ac_shuffled_footprint_res_${res}_tile_${tile}_${i}.txt

  #PolII
  bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=PolII $shuffled_dir/breakpoints_all_shuffled_${i}.bed $GSE60024_dir/RPE-POLII/${signal_dir}/SRR1534810.nodup.tagAlign_x_SRR1534805.nodup.tagAlign.fc.signal.bw $perm_dir/polII_shuffled_footprint_res_${res}_tile_${tile}_${i}.txt
done

cat $perm_dir/ctcf_shuffled_footprint_res_${res}_tile_${tile}_*.txt | gzip - > $out_dir/ctcf_shuffled_footprint_res_${res}_tile_${tile}.txt.gz
cat $perm_dir/h3k27ac_shuffled_footprint_res_${res}_tile_${tile}_*.txt | gzip - > $out_dir/h3k27ac_shuffled_footprint_res_${res}_tile_${tile}.txt.gz
cat $perm_dir/polII_shuffled_footprint_res_${res}_tile_${tile}_*.txt | gzip - > $out_dir/polII_shuffled_footprint_res_${res}_tile_${tile}.txt.gz

#RNA-seq

#C29
bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=C29_R1,C29_R2 breakpoints_C29.bed $wig_dir/C29/C29_r1.bw,$wig_dir/C29/C29_r2.bw $out_dir/rna_C29_breaks_footprint_res_${res}_tile_${tile}.txt
#bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=C29_R1,C29_R2 breakpoints_C29_shuffled.bed $wig_dir/C29/C29_r1.bw,$wig_dir/C29/C29_r2.bw rna_C29_shuffled_footprint_res_${res}_tile_${tile}.txt

#C93
bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=C93_R1,C93_R2 breakpoints_all.bed $wig_dir/C93/C93_r1.bw,$wig_dir/C93/C93_r2.bw $out_dir/rna_C93_breaks_footprint_res_${res}_tile_${tile}.txt
#bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=C93_R1,C93_R2 breakpoints_all_shuffled.bed $wig_dir/C93/C93_r1.bw,$wig_dir/C93/C93_r2.bw rna_C93_shuffled_footprint_res_${res}_tile_${tile}.txt

#DCB2
bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=DCB2_R1,DCB2_R2 breakpoints_DCB2.bed $wig_dir/DCB2/DCB2_r1.bw,$wig_dir/DCB2/DCB2_r2.bw $out_dir/rna_DCB2_breaks_footprint_res_${res}_tile_${tile}.txt
#bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=DCB2_R1,DCB2_R2 breakpoints_DCB2_shuffled.bed $wig_dir/DCB2/DCB2_r1.bw,$wig_dir/DCB2/DCB2_r2.bw rna_DCB2_shuffled_footprint_res_${res}_tile_${tile}.txt

for i in {1..100};
do
  bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=C93_R1,C93_R2 $shuffled_dir/breakpoints_all_shuffled_${i}.bed $wig_dir/C93/C93_r1.bw,$wig_dir/C93/C93_r2.bw $perm_dir/rna_C93_shuffled_footprint_res_${res}_tile_${tile}_${i}.txt
  bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=C29_R1,C29_R2 $shuffled_dir/breakpoints_C29_shuffled_${i}.bed $wig_dir/C29/C29_r1.bw,$wig_dir/C29/C29_r2.bw $perm_dir/rna_C29_shuffled_footprint_res_${res}_tile_${tile}_${i}.txt
  bwtool matrix ${res}:${res} -tiled-averages=$tile -long-form=DCB2_R1,DCB2_R2 $shuffled_dir/breakpoints_DCB2_shuffled_${i}.bed $wig_dir/DCB2/DCB2_r1.bw,$wig_dir/DCB2/DCB2_r2.bw $perm_dir/rna_DCB2_shuffled_footprint_res_${res}_tile_${tile}_${i}.txt
done

cat $perm_dir/rna_C93_shuffled_footprint_res_${res}_tile_${tile}_*.txt | gzip  - > $out_dir/rna_C93_shuffled_footprint_res_${res}_tile_${tile}.txt.gz
cat $perm_dir/rna_C29_shuffled_footprint_res_${res}_tile_${tile}_*.txt | gzip  - > $out_dir/rna_C29_shuffled_footprint_res_${res}_tile_${tile}.txt.gz
cat $perm_dir/rna_DCB2_shuffled_footprint_res_${res}_tile_${tile}_*.txt | gzip  - > $out_dir/rna_DCB2_shuffled_footprint_res_${res}_tile_${tile}.txt.gz

rm $perm_dir/*
