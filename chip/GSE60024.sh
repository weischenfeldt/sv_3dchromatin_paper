#!/bin/bash

#PBS -l nodes=1:ppn=8:thinnode,walltime=48:00:00
#PBS -W group_list=cu_10027 -A cu_10027
#PBS -m n
#PBS -j eo
#PBS -N chipseq
#PBS -V

module purge
module load torque moab tools gcc/5.4.0
module load anaconda/2.2.0
source activate aquas_chipseq

export PATH=$HOME/.bds:$PATH

sra=$1
sample=$2
type=$3

fastq_dir=/home/projects/cu_10027/projects/rpe/data/data_raw/chipseq/GSE60024
base_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/chipseq/GSE60024

mkdir -p $base_dir/${sample}/aquas_chipseq
cd $base_dir/${sample}/aquas_chipseq

python /home/projects/cu_10027/apps/software/TF_chipseq_pipeline/chipseq.py \
  --species hg38 --type $type --se --title GSE60024_${sample} \
  --fastq1 $fastq_dir/${sra}.fastq.gz \
  --ctl_fastq1 $fastq_dir/SRR1534805.fastq.gz \
  --ctl_fastq2 $fastq_dir/SRR1534807.fastq.gz \
  --out-dir $base_dir/${sample}/aquas_chipseq --system local --nth 8 \
  --final-stage idr
