#!/bin/bash

chr=chr12p
start=45755000
end=45995000

base_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/juicer/work/HiCBM178/aligned/chromothripsis/diploid
draft=$base_dir/duplicated/hg38.diploid.2_12_22.dups.draft.assembly

export mnd=$base_dir/mat_pat_2_12_22.sorted.txt
export dup=$base_dir/duplicated/mat_pat_2_12_22.dups.txt
dup_mnd=$base_dir/duplicated/mat_pat_2_12_22.dups.mnd.txt

get_dups() {
  chr=$1
  start=$2
  end=$3

`  awk -v chr=$chr -v min=$start -v max=$end '{
    if ($3==chr && $7==$3) {
      if (($4 >= min && $4 <= max) && ($8 >= min && $8 <= max)) {
        print $1,$2,chr"_dup:"min"_"max,$4-min,$5,$6,chr"_dup:"min"_"max,$8-min,$9,$10,$11
      } else if ($4 >= min && $4 <= max) {
        print $1,$2,chr"_dup:"min"_"max,$4-min,$5,$6,$7,$8,$9,$10,$11
      } else if ($8 >= min && $8 <= max) {
        print $1,$2,$3,$4,$5,$6,chr"_dup:"min"_"max,$8-min,$9,$10,$11
      }
    } else if ($3==chr && $4 >= min && $4 <= max) {
      print $1,$2,chr"_dup:"min"_"max,$4-min,$5,$6,$7,$8,$9,$10,$11
    } else if ($7==chr && $8 >= min && $8 <= max) {
      print $1,$2,$3,$4,$5,$6,chr"_dup:"min"_"max,$8-min,$9,$10,$11
    }}' $mnd >> $dup
}

export -f get_dups

get_dups chr12p 45755000 45995000
get_dups chr12p 46270000 46370000
get_dups chr12p 82190000 82345000`

awk '{if ($3 > $7) print $1,$6,$7,$8,$9,$2,$3,$4,$5,$11,$10; else print $0}' $dup > tmp

cat $mnd tmp | awk '{print $2,$3,$4,$5,$6,$7,$8,$9,$10,".","R1",$11,".","R2",$1,$1}' - | \
  sort --parallel=10 -k2,2d -k6,6d -k4,4n -k8,8n -k1,1n -k5,5n -k3,3n > $dup_mnd

bash /home/projects/cu_10027/apps/software/3d-dna/visualize/run-assembly-visualizer.sh -n -r 2500000,1000000,500000,250000,100000,50000,25000,10000,5000 -p false -q 30 $draft $dup_mnd
