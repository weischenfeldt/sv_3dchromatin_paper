#!/bin/bash
#PBS -l nodes=1:ppn=14:thinnode,walltime=24:00:00
#PBS -W group_list=cu_10027 -A cu_10027
#PBS -m n
#PBS -j eo
#PBS -N visualize_assembly
#PBS -V

module load tools
module load parallel/20170822
module load java/1.8.0
module load pigz/2.3.3

sample=$1

scripts=/home/projects/cu_10027/apps/software/3d-dna
work_dir=/home/projects/cu_10027/projects/rpe/data/data_processed/hg38/juicer/work/$sample/aligned
mnd=$work_dir/merged_nodups.txt.gz
draft=$scripts/drafts/hg38.draft.assembly

cd $work_dir

pigz -p 14 -d $mnd
mnd=$work_dir/merged_nodups.txt

mkdir -p $work_dir/assembly
cd $work_dir/assembly

bash /home/projects/cu_10027/apps/software/3d-dna/visualize/run-assembly-visualizer.sh -q 30 $draft $mnd

cd $work_dir
pigz -p 14 $mnd
