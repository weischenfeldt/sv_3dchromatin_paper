# README #

## Code used for the analysis in Sidiropoulos & Mardin et al. ##
Code used for the data analyses in Sidiropoulos & Marding et al. paper "Somatic structural variant formation is guided by and influences genome architecture".

The project involved Hi-C, WGS (whole genome sequencing), mate-pair, RNA-seq, ChIP-seq Repli-seq from wild type, TP53 KO and doxorubicin treated RPE-1 cells.
The code is authored by Nikos Sidiropoulos and provided "as is", in the hope that it will be useful, but without warranty of any kind. The code is organized in:

* `assembly` - scripts used to produce the BM178 partial chromosome 12 assembly with Juicebox Assembly Tools (JBAT)
* `chip` - processing scripts for C29 H3K4me3 (this study) and WT RPE-1 H3K27ac, CTCF and RNA PolII acquired from GSE60024 
* `hic`
* `integrative`
* `matepair`
* `phasing`
* `replication`
* `rna`
* `wgs`

Software versions are listed in the Methods section and the Reporting Summary linked to the article.

For questions and requests please contact Nikos Sidiropoulos <nikos.sidiropoulos(at)bric.ku.dk>.

